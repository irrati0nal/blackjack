#include <iostream>
#include "MiniCppUnit.hxx"

#include "Card.hxx" 

class CardTests : public TestFixture<CardTests>
{
public:
	TEST_FIXTURE( CardTests ){
			TEST_CASE( testCreateCard );
	}

	void testCreateCard(){
		
		Card *c1 = new Card("Hearts-4",4);

		ASSERT_EQUALS( "Hearts-4", c1->getName() );
		ASSERT_EQUALS( 4, c1->getValue() );

		delete c1;
	}
};

REGISTER_FIXTURE(CardTests)
