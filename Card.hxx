#ifndef Card_hpp
#define Card_hpp

#include <string>

class Card
{
public:
	Card(std::string name, unsigned int value) : name(name), value(value) {};
	std::string getName() { return name; }
	int getValue() { return value; }

private:
	std::string name;
	int value;

};

#endif


